SUMMARY = "UVC gadget userspace sample application"
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://uvc-gadget.c;beginline=1;endline=18;md5=414860c3c534dc95d81da9564cfb8d2a"

DEPENDS = "virtual/kernel"

SRC_URI = "git://github.com/cyboflash/uvc-gadget.git \
    file://0001-Fix-low-fps-intervals-off-by-a-factor-of-10.patch \
    file://0002-Add-1080p-and-60fps-frame-descriptors.patch \
    file://0003-Set-fps-parameter-of-source-when-starting-stream.patch \
    file://0004-Allow-driver-to-handle-bulk-STREAMON-STREAMOFF-event.patch \
    file://0005-Don-t-quit-on-select-timeout.patch \
    file://0006-Improve-error-handling-in-main-loop.patch \
    file://0007-uvc_video_process-Improve-buffer-dequeing-logic.patch \
    file://0008-Allow-mplane-capture-temp-for-vip-compatibility.patch \
    file://0009-Set-uvc-format-on-open.patch \
    file://0010-Round-dwMaxPayloadSize-down-to-a-multiple-of-64k-for.patch \
    file://0011-Improve-streamoff-handling-in-bulk-mode.patch \
    file://0012-Catch-SIGINT-to-properly-shut-down.patch \
"

PV = "1.0+git${SRCPV}"
SRCREV = "ff349f19b69950669c4ef9c8eb92e760ea45fdea"

S = "${WORKDIR}/git"

do_compile () {
	${CC} ${CFLAGS} -I${STAGING_KERNEL_DIR} ${LDFLAGS} -o uvc-gadget uvc-gadget.c
}

do_install () {
	install -d ${D}${bindir}
	install -m755 uvc-gadget ${D}${bindir}
}
