SRC_URI += "file://bsp.cfg \
            file://user_2018-01-31-12-15-00.cfg \
            file://user_2018-01-31-12-51-00.cfg \
            "

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://0001-usb-dwc3-fix-for-isocronous-transfer-performance-inc.patch \
    file://0001-usb-gadget-f_uvc-Fix-SuperSpeed-companion-descriptor.patch \
    file://0001-usb-gadget-webcam-Add-more-frame-intervals-through-1.patch \
    file://0001-vivid-Allow-any-frame-interval-for-all-resolutions.patch \
    file://0001-function-uvc-Add-bulk-support.patch \
    file://0002-function-uvc-Change-set_alt-behavior-for-bulk-mode.patch \
    file://0001-usb-gadget-function-Add-bulk-endpoint-max-payload-mu.patch \
    file://0001-usb-gadget-function-uvc_video-Also-handle-ECONNRESET.patch \
    "

